/**
 * 
 */
package org.guscastles.structures;

/**
 * Hash table class, providing storage for String objects.
 * 
 * @author gustavo
 * 
 */
public class HashTable {

   /**
    * Internal storage of key/value pairs, with hash codes as the main array.
    */
   private String[][] keys;
   private String[][] values;

   /**
    * Default constructor.
    */
   public HashTable() {
   }

   /**
    * Adds the given value to this hash table for the given key. If the key does
    * not exist, it is created.
    * 
    * @param key
    *           The key to insert the new value under.
    * @param value
    *           The new value.
    */
   public void put(String key, String value) {
      int hashCode = getHashCode(key);
      if (keys == null || keys.length - 1 < hashCode) {
         includNewKeyAndValue(hashCode, key, value);
      } else {
         updateKeyAndValue(hashCode, key, value);
      }
   }

   /**
    * Adds a new key and value to the internal storage.
    * 
    * @param hashCode
    *           The hash code for the key.
    * @param key
    *           The key value.
    * @param value
    *           The actual value to be included.
    */
   private void includNewKeyAndValue(int hashCode, String key, String value) {
      if (keys == null) {
         keys = new String[hashCode + 1][0];
         values = new String[hashCode + 1][0];
      }
      String[][] updatedKeys = new String[hashCode + 1][0];
      String[][] updatedValues = new String[hashCode + 1][0];
      for (int idx = 0; idx < keys.length; ++idx) {
         updatedKeys[idx] = keys[idx];
         updatedValues[idx] = values[idx];
      }
      updatedKeys[hashCode] = new String[] { key };
      updatedValues[hashCode] = new String[] { value };
      keys = updatedKeys;
      values = updatedValues;
   }

   /**
    * Updates the value for the given key, if the key exists.
    * 
    * @param hashCode
    *           The hash code for the key.
    * @param key
    *           The key value.
    * @param value
    *           The actual value to be included.
    */
   private void updateKeyAndValue(int hashCode, String key, String value) {
      String[] hashCodeKeys = keys[hashCode];
      String[] hashCodeValues = values[hashCode];
      String[] keysUnderHashCode = new String[hashCodeKeys.length + 1];
      String[] valuesUnderHashCode = new String[hashCodeValues.length + 1];
      int idx = -1;
      int i = 0;
      for (String hashCodeKey : hashCodeKeys) {
         if (hashCodeKey.equals(key)) {
            idx = i;
            break;
         }
         i++;
      }
      if (idx < 0) {
         keysUnderHashCode[keysUnderHashCode.length - 1] = key;
         valuesUnderHashCode[keysUnderHashCode.length - 1] = value;
      } else {
         values[hashCode][i] = value;
      }
   }

   /**
    * Removes the given element from this hash table.
    * 
    * @param element
    *           The element to be removed.
    */
   public void remove(String key) {
      int hashCode = getHashCode(key);
      if (keys != null && keys.length - 1 >= hashCode) {
         String[] existingKeys = keys[hashCode];
         String[] updatedExistingKeys = new String[existingKeys.length - 1];
         String[] existingValues = values[hashCode];
         String[] updatedExistingValues = new String[existingValues.length - 1];
         int newIdx = 0;
         for (int i = 0; i < existingKeys.length; ++i) {
            if (!existingKeys[i].equals(key)) {
               updatedExistingKeys[newIdx++] = existingKeys[i];
               updatedExistingValues[newIdx++] = existingValues[i];
            }
         }
         keys[hashCode] = updatedExistingKeys;
         values[hashCode] = updatedExistingValues;
      }
   }

   /**
    * Retrieves the value held by the given element from this hash table.
    * 
    * @param element
    *           The element to search for.
    * @return The value that the given element holds.
    */
   public String get(String key) {
      int hashCode = getHashCode(key);
      int idx = -1;
      if (keys != null && hashCode <= keys.length - 1) {
         int i = 0;
         for (String availableKey : keys[hashCode]) {
            if (availableKey.equals(key)) {
               idx = i;
               break;
            }
            i++;
         }
      }
      if (idx == -1) {
         return null;
      } else {
         return values[hashCode][idx];
      }
   }

   /**
    * The String representation of this hash table.
    */
   public String toString() {
      StringBuffer sb = new StringBuffer();
      if (keys != null) {
         sb.append('[');
         for (int i = 0; i < keys.length; ++i) {
            String[] existingKeys = keys[i];
            String[] existingValues = values[i];
            for (int j = 0; j < existingKeys.length; ++j) {
               int length = sb.length();
               if (length > 1) {
                  sb.append(", ");
               }
               sb.append(existingKeys[j]);
               sb.append(": ");
               sb.append(existingValues[j]);
            }
         }
         sb.append(']');
      }
      return sb.toString();
   }

   /**
    * Gets the hash code for the given key.
    * 
    * @param key
    *           The key to be hashed.
    * @return An integer representing this key.
    */
   private int getHashCode(String key) {
      char[] chars = key.toCharArray();
      int hashCode = 0;
      for (char ch : chars) {
         hashCode += (int) ch;
      }
      return hashCode / 17;
   }

   /**
    * Retrieves the list of keys.
    * 
    * @return
    */
   public String[] getKeyList() {
      int size = 0;
      for (String[] existingKeys : keys) {
         size += existingKeys.length;
      }
      int i = 0;
      String[] keyList = new String[size];
      for (String[] existingKeys : keys) {
         for (String keyValue : existingKeys) {
            keyList[i++] = keyValue;
         }
      }
      return keyList;
   }

   /**
    * Returns the size of the current key list.
    * 
    * @return The size of the current key list.
    */
   public int size() {
      int size = 0;
      if (keys != null) {
         for (String[] existingKeys : keys) {
            size += existingKeys.length;
         }
      }
      return size;
   }
}
