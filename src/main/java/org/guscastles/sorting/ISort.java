package org.guscastles.sorting;

public interface ISort {

	int [] sort(int [] array, int first, int last, int initial);
}
