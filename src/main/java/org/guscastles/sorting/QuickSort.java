package org.guscastles.sorting;

public class QuickSort implements ISort {

   /**
    * Registers the number of comparisons of this algorithm.
    */
   private static int numberOfComparisons;

   public int[] sort(int[] array, int index, int last, int pivot) {
      if (index >= pivot) {
         return array;
      }
      int i = index;
      while (i < pivot) {
         if (++numberOfComparisons > 0 && array[i] > array[pivot]) {
            array = move(array, i, pivot);
            pivot = pivot - 1;
         } else {
            ++i;
         }
      }
      array = sort(array, index, 0, pivot - 1);
      array = sort(array, pivot + 1, 0, array.length - 1);
      return array;
   }

   /**
    * Bubble sort method to change the positions of two given elements.
    * 
    * @param array
    *           The array to be changed.
    * @param move
    *           The first element.
    * @param pivot
    *           The second element.
    * @return The modified array after the change.
    */
   private int[] move(int[] array, int move, int pivot) {
      for (int i = move; i <= pivot; ++i) {
         System.out.print(array[i] + ", ");
      }
      System.out.println();
      int currentMove = array[move];
      array[move] = array[pivot - 1];
      array[pivot - 1] = array[pivot];
      array[pivot] = currentMove;
      for (int i = move; i <= pivot; ++i) {
         System.out.print(array[i] + ", ");
      }
      System.out.println();
      return array;
   }

   public static void setNumberOfComparisons(int newCounter) {
      QuickSort.numberOfComparisons = newCounter;
   }

   public int getNumberOfComparisons() {
      return numberOfComparisons;
   }
}
