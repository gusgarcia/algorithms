/**
 * 
 */
package org.guscastles.structures;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author gustavo
 * 
 */
public class HashTableTest {

   private static final int TEST_HASH_CODE = 49;
   private static final String TEST_KEY = "element10";
   private static final String TEST_VALUE = "Hello world!";

   private HashTable hashTable;

   /**
    * @throws java.lang.Exception
    */
   @BeforeClass
   public static void setUpBeforeClass() throws Exception {
   }

   /**
    * @throws java.lang.Exception
    */
   @AfterClass
   public static void tearDownAfterClass() throws Exception {
   }

   /**
    * @throws java.lang.Exception
    */
   @Before
   public void setUp() throws Exception {
      hashTable = new HashTable();
   }

   /**
    * @throws java.lang.Exception
    */
   @After
   public void tearDown() throws Exception {
   }

   /**
    * Test method for
    * {@link org.guscastles.structures.HashTable#add(java.lang.String)}.
    */
   @Test
   public final void testPutAntGet() {
      hashTable.put(TEST_KEY, TEST_VALUE);
      Assert.assertEquals(TEST_VALUE, hashTable.get(TEST_KEY));
      hashTable.put(TEST_KEY + "2", TEST_VALUE + "2");
      Assert.assertEquals(TEST_VALUE + "2", hashTable.get(TEST_KEY + "2"));
   }

   /**
    * Test method for private String[][] keyValues;
    * 
    * {@link org.guscastles.structures.HashTable#remove(java.lang.String)}.
    */
   @Test
   public final void testPutRemoveAndGet() {
      hashTable.put(TEST_KEY, TEST_VALUE);
      Assert.assertEquals(TEST_VALUE, hashTable.get(TEST_KEY));
      hashTable.remove(TEST_KEY);
      Assert.assertNull(hashTable.get(TEST_KEY));
   }

   /**
    * Test method for private String[][] keyValues;
    * 
    * {@link org.guscastles.structures.HashTable#remove(java.lang.String)}.
    */
   @Test
   public final void testPutAndRemoveNonExistingKey() {
      hashTable.put(TEST_KEY, TEST_VALUE);
      hashTable.remove(TEST_KEY + "2");
      Assert.assertNotNull(hashTable.get(TEST_KEY));
   }

   /**
    * Test method for private String[][] keyValues;
    * 
    * {@link org.guscastles.structures.HashTable#getKeyList(java.lang.String)}.
    */
   @Test
   public final void testPutAndGetKeyList() {
      hashTable.put(TEST_KEY, TEST_VALUE);
      hashTable.put(TEST_KEY + "2", TEST_VALUE + "2");
      String[] keys = hashTable.getKeyList();
      Assert.assertEquals(TEST_KEY, keys[0]);
      Assert.assertEquals(TEST_KEY + "2", keys[1]);
   }

   /**
    * Test method for {@link org.guscastles.structures.HashTable#toString()}.
    */
   @Test
   public final void testPutAndToString() {
      hashTable.put(TEST_KEY, TEST_VALUE);
      hashTable.put(TEST_KEY + "2", TEST_VALUE + "2");
      String hashTableAsString = hashTable.toString();
      Assert.assertNotEquals("", hashTableAsString);
      Assert.assertEquals(
            "[element10: Hello world!, element102: Hello world!2]",
            hashTableAsString);
   }

   /**
    * Test method for
    * {@link org.guscastles.structures.HashTable#put(java.lang.String,java.lang.String)}
    * .
    */
   @Test
   public final void testPutAndSize() {
      Assert.assertEquals(0, hashTable.size());
      hashTable.put(TEST_KEY, TEST_VALUE);
      hashTable.put(TEST_KEY + "2", TEST_VALUE + "2");
      Assert.assertEquals(2, hashTable.size());
   }

   /**
    * Test method for
    * {@link org.guscastles.structures.HashTable#get(java.lang.String)}.
    */
   @Test
   public final void testGetNonExistingKey() {
      String value = hashTable.get(TEST_KEY);
      Assert.assertNull(value);
   }

   /**
    * Test method for
    * {@link org.guscastles.structures.HashTable#get(java.lang.String)}.
    */
   @Test
   public final void testPutAndGetNonExistingKey() {
      hashTable.put(TEST_KEY, TEST_VALUE);
      String value = hashTable.get(TEST_KEY + "2");
      Assert.assertNull(value);
   }

}
