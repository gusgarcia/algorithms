/**
 * 
 */
package org.gusccastles.sorting;

import static org.junit.Assert.assertEquals;

import org.guscastles.sorting.QuickSort;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author gustavo
 * 
 */
public class QuickSortTest {

   private QuickSort quickSort;
   int[] array;
   int pivot;
   int last;
   int first;

   /**
    * @throws java.lang.Exception
    */
   @BeforeClass
   public static void setUpBeforeClass() throws Exception {
   }

   /**
    * @throws java.lang.Exception
    */
   @AfterClass
   public static void tearDownAfterClass() throws Exception {
   }

   /**
    * @throws java.lang.Excepti0on
    */
   @Before
   public void setUp() throws Exception {
      array = new int[] { 5, 4, 1, 3, 2 };
      quickSort = new QuickSort();
      pivot = array.length - 1;
      last = 3;
      // first = 0;
      QuickSort.setNumberOfComparisons(0);
   }

   /**
    * @throws java.lang.Exception
    */
   @After
   public void tearDown() throws Exception {
      System.out.println("n: " + array.length + "/nlogn: "
            + Double.valueOf(Math.log(array.length) * array.length).intValue()
            + "/n^2: " + array.length * array.length + "/actual: "
            + +quickSort.getNumberOfComparisons());
   }

   /**
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort0() {
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(1, array[0]);
   }

   /**
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort1() {
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(2, array[1]);
   }

   /**
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort2() {
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(3, array[2]);
   }

   /**
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort3() {
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(4, array[3]);
   }

   /**
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort4() {
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(5, array[4]);
   }

   /**
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort5() {
      array = new int[] { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
      pivot = array.length - 1;
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(9, array[array.length - 1]);
      Assert.assertTrue(Integer.toString(array[8]),
            array[array.length - 1] == 9);
      Assert.assertTrue(Integer.toString(array[4]), array[4] == 5);
   }

   /**
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort6() {
      array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
      pivot = array.length - 1;
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(10, array[array.length - 1]);
      Assert.assertTrue(Integer.toString(array[8]),
            array[array.length - 2] == 9);
      Assert.assertTrue(Integer.toString(array[4]), array[4] == 5);
   }

   /**
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort7() {
      array = new int[] { 45, 435, 2345, 3, 4, 345, 77, 2, 33, 67, 987, 90, 4,
            1, 3, 4, 10 };
      pivot = array.length - 1;
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(2345, array[array.length - 1]);
      Assert.assertTrue(Integer.toString(array[0]), array[0] == 1);
      Assert.assertTrue(array[2] == array[3]);
   }

   /**
    * array.length - 1;
    * 
    * Test method for
    * {@link org.guscastles.sorting.QuickSort#quickSort(int[], int, int, int)}.
    */
   @Test
   public void testQuickSort8() {
      array = new int[] { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
            4, 4, 4, 4, 4, 4 };
      pivot = array.length - 1;
      array = quickSort.sort(array, 0, last, pivot);
      assertEquals(4, array[array.length - 1]);
      Assert.assertTrue(Integer.toString(array[8]), array[0] == 4);
      Assert.assertTrue(array[2] == array[3]);
   }
}
